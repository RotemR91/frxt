import {
  ExtractorFunction,
  Logic,
  Filter,
  JSONSQL,
  RecordId,
  ExtractorConfig,
  Record, Field, Fields, TokenMapConfig,
  CNFLogicConfig,
  CNFFunctionConfig,
  CNFConditionalConfig,
} from './types'
import {jsonFields} from './jsonsql'


/**
 * @function identity
 * the identity function, returns the value of the speccified field as is from
 * the record.
 * @param {RecordId} id - raw text to try and match.
 * @param {Field} field - SQL-esque JSON data.
 * @param {Record} record - a subset of record ids from the provided json data
 * @returns {any} value - record[field]
 */
export const identity:ExtractorFunction =
function(id:string|number, field:string, record:object) {
  return record[field]
}

/**
 * @function lookup
 * Attemps to extract object[key], however if that is undefined returns fallback
 * @param {object} object - object to search
 * @param {string} key - key to extract
 * @param {any} fallbak - default value to use if object[key] is undefined
 * @returns {any} value - record[field]
 */
export const lookup =
function(object:object, key:string, fallback): any {
  return (key in object)
    ? object[key]
    : fallback
}


/**
 * @function hasSelectedValidOption
 * Attemps to determine if the provided object is of the known options
 * @param {string} selected - option to search for
 * @param {string[]|object} options - known options
 * @returns {boolean} whether or not select is in options
 */
export const hasSelectedValidOption =
function(selected:string, options:object|string[]): boolean {
  return Array.isArray(options)
    ? options.indexOf(selected) > -1
    : selected in options;
}

export const defaultFieldRenderFunction = identity
export const defaultFieldSortByFunction = identity
export const defaultFieldFilterFunction = identity

/**
 * @function isObjectEmpty
 * Whether or not the object has any keys
 * @param {object} obj - the objec to test
 * @returns {boolean} whether or not obj is empty
 */
export function isObjectEmpty(obj): boolean {
  return (
    Object.entries(obj).length === 0 &&
    obj.constructor === Object
  )
}


export function sleep(milliseconds:number) {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}


export function uuidv4() : string {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}


/**
 * @function makeDefaultTknFieldMap
 * Uses all fields from the json data to make a default token map
 * @param {JSONSQL} json - SQL-esque JSON data
 * @returns {TokenMapConfig} the token mapping for fields ({x:x})
 */
export const makeDefaultTknFieldMap =
function(json:JSONSQL) {
  let map = {}
  let fields = jsonFields(json)
  fields.forEach(field=>{
    map[field]=field
  })
  return map
}

export function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		}, wait);
		if (immediate && !timeout) func.apply(context, args);
	};
}


/**
 * @function isLogic
 * Determines which if provided argument is known logic
 * @param {x} any
 * @returns {boolean}
 */
export function isLogic(x: any): x is Logic {
  return ['and', 'or'].indexOf(x) > -1;
}


/**
 * @function isFilter
 * Determines which if provided argument has the shape of a filter
 * @param {x} any
 * @returns {boolean}
 */
export const isFilter =
function(x:any): x is Filter {
  if (typeof x !== 'object') return false
  return [
    isLogic(x.logic),
    typeof x.function === 'string',
    typeof x.field === 'string',
    typeof x.conditional === 'string',
    typeof x.input === 'string'
  ].every(x=>x)
}


/**
 * @function isValidFilter
 * Determines which if given filter is valid
 * @param {JSONSQL} json - an SQL-like object
 * @param {Fields} validFieldTokens - known fields
 * @param {CNFLogicConfig} configLogic - conjunctive normal form logic
 * configuration.
 * @param {CNFFunctionConfig} configFunctions - conjunctive normal form function
 * configuration, i.e. what functions are avaliable to be applied.
 * @param {CNFConditionalConfig} configConditionals - conjunctive normal form
 * conditional configuration, i.e. what conditionals are avaliable to be applied.
 * @returns {boolean}
 */
export const isValidFilter =
function(
  filter:any,
  validFieldTokens:Fields,
  cnfLogicConfig:CNFLogicConfig,
  cnfFunctionsConfig:CNFFunctionConfig,
  cnfConditionalConfig:CNFConditionalConfig,
): boolean {
  if (!isFilter(filter)) return false
  return [
    hasSelectedValidOption(filter.logic, cnfLogicConfig),
    hasSelectedValidOption(filter.field, validFieldTokens),
    hasSelectedValidOption(filter.function, cnfFunctionsConfig),
    hasSelectedValidOption(filter.conditional, cnfConditionalConfig),
    filter.input !== ''
  ].every(x => x === true);
}


/**
 * @function isRecordSortable
 * Determines which if given record can be sorted on specified fields
 * @param {JSONSQL} json - an SQL-like object
 * @param {RecordId} id - the record id under consideration
 * @param {array} fields - the fields on which to sort
 * @param {ExtractorConfig} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, field, record)
 * @returns {boolean}
 */
export const isRecordSortable =
function(
  json:JSONSQL,
  id:RecordId,
  fields:Fields,
  fieldSortByFunctions:ExtractorConfig={}
): boolean {
  let anyMissing = fields.some(field => {
    let extractor = (field in fieldSortByFunctions) ? fieldSortByFunctions[field] : identity
    let value = extractor(id, field, json[id])
    return (typeof value  === 'undefined' || value == null)
  })
  return !anyMissing
}



/**
 * @function reverseLookup
 * Checks to values of map for the provided value whereby the key is also
 * not the provide value. If not found will return value by default.
 * This is useful when trying to find a synonym for a field, that is not the field.
 * @param {TokenMapConfig} map - an SQL-like object
 * @param {Field} value - the record id under consideration
 * @returns {Field}
 */
export const reverseLookup =
function(map:TokenMapConfig, value:Field): Field {
  let found = false
  for (let key in map) {
    let val = map[key] as Field
    if (val === value) {
      found = true
      if (key !== value) {
      return key as Field
      }
    }
  }
  return value
}

export default {
  identity,
  lookup,
  hasSelectedValidOption,
  isObjectEmpty,
  sleep,
  uuidv4,
  makeDefaultTknFieldMap,
  debounce,
  isLogic,
  isFilter,
  isValidFilter,
  isRecordSortable,
  reverseLookup
}
